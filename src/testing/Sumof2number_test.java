package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sample.TwoNumberSum;

public class Sumof2number_test {


	@Test
	public void test() {
		ArrayList<Integer> input = new ArrayList();
		input.add(1);
		input.add(2);
		input.add(3);
		input.add(4);
		input.add(5);
		input.add(6);
		input.add(7);
		input.add(8);
		input.add(9);
		input.add(10);
		
		ArrayList<List<Integer>> output = TwoNumberSum.INSTANCE.getTwoNumberSumPair(input, 5);
		
		for (int i=0; i< output.size(); i++){
			System.out.println("Set "+ (i+1));
			
			for(Integer value: output.get(i)){
				System.out.print(value + " ");
			}
			System.out.println("\n");
		}
		
	}

}
