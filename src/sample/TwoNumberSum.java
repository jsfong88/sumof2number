package sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public enum TwoNumberSum {
	INSTANCE;
	
	public ArrayList<List<Integer>> getTwoNumberSumPair(List <Integer>input, int target){
		
		ArrayList<List<Integer>> outputPair = new ArrayList<List<Integer>>();
		
		HashMap hm = new HashMap();
		
		//Iterator input and form the hashmap
		for(Integer i: input){
			int key = i;
			int value = target - i;			
			if(value > 0){
				hm.put(key, value);
			}
		}
		
		//Iterator hashmap to form pair		
		Iterator i = hm.entrySet().iterator();
		
		while(i.hasNext()){
			Map.Entry pair = (Map.Entry)i.next();
			Integer key = (Integer) pair.getValue();			
			Integer value = (Integer) hm.get(key);
			
			if(value != null){
				List<Integer> numberPair = new ArrayList();
				numberPair.add(key);
				numberPair.add(value);
				outputPair.add(numberPair);
			}
		}
		
		return outputPair;
	}

}
