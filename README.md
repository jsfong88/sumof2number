**Sum of 2 number**

You are given a list of numbers and a target number. Find all combinations of two numbers that sum
up to the target number.
Bonus: Think about how you can do this in a single pass for each number (hint: hash map)
EXAMPLE:
List<int> NumSet = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
int Target = 5
Ans:
[1, 4]
[2, 3]